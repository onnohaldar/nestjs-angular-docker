import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';

const matModules = [
    FlexLayoutModule,
    MatIconModule,
    MatToolbarModule,
];

@NgModule({
    imports: [matModules],
    exports: [matModules]
})
export class AppMaterialModule { }